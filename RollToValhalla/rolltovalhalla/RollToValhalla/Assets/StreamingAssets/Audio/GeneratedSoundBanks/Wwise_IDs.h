/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BOULDER_ROLLING_LOOP = 3990669547U;
        static const AkUniqueID BOULDERFALL = 1827684087U;
        static const AkUniqueID BOULDERHITSTREE = 1467160966U;
        static const AkUniqueID BRANCHDOOR = 2579815453U;
        static const AkUniqueID FOREST_SOUNDSCAPE_LOOP = 59657081U;
        static const AkUniqueID MAINFOOTSTEPS = 3525254671U;
        static const AkUniqueID MAINVOICE_IDLE = 994578425U;
        static const AkUniqueID MANGETSKILLED_SLOW = 1414978409U;
        static const AkUniqueID MANHITSSURFACE_TREE = 2462023217U;
        static const AkUniqueID MANHITSSURFACE_WALL = 1248730809U;
        static const AkUniqueID MUSICFOREST = 840410985U;
        static const AkUniqueID MUSICPRISON = 1308100511U;
        static const AkUniqueID PRISON_SOUNDSCAPE_LOOP = 699158023U;
        static const AkUniqueID PRISONDOOR = 2647673640U;
        static const AkUniqueID UI_CLICK = 2249769530U;
        static const AkUniqueID VALKYRIEFAST_FOOTSTEPS = 2956008112U;
        static const AkUniqueID VALKYRIEFAST_VOICE = 804847619U;
        static const AkUniqueID VALKYRIESLOW_FOOTSTEPS = 3930970313U;
        static const AkUniqueID VALKYRIESLOW_VOICE = 418378138U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace LEVEL
        {
            static const AkUniqueID GROUP = 2782712965U;

            namespace STATE
            {
                static const AkUniqueID FOREST = 491961918U;
                static const AkUniqueID PRISON = 175603640U;
            } // namespace STATE
        } // namespace LEVEL

    } // namespace STATES

    namespace SWITCHES
    {
        namespace VALKYRIE_KILL
        {
            static const AkUniqueID GROUP = 2315582845U;

            namespace SWITCH
            {
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID SLOW = 787604482U;
            } // namespace SWITCH
        } // namespace VALKYRIE_KILL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BOULDER_VELOCITY = 3260431906U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUSIC_VOLUME = 1006694123U;
        static const AkUniqueID SOUND_VOLUME = 495870151U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SOUND = 623086306U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
