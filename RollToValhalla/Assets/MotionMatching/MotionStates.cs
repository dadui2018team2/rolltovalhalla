﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class MotionStates : MonoBehaviour {

    [Header("Database files")]
    public TextAsset database_idle;
    public TextAsset database_forward;
    public TextAsset database_left;
    public TextAsset database_right;

    public bool generateDatabases = false;

    private char previousState = 'i';
    private char currentState = 'i';

    private int _framerate = 30;
    private int _boneCount = 26; //26 is the cleaned rig, 65 is the original Rokoko

    private int totalFrames_idle = 71;
    private int totalFrames_forward = 31;
    private int totalFrames_left = 34;
    private int totalFrames_right = 34;

    public List<MotionState> motionStates_ALL = new List<MotionState>();
    public List<MotionState> motionStates_IDLE = new List<MotionState>();
    public List<MotionState> motionStates_FORWARD = new List<MotionState>();
    public List<MotionState> motionStates_LEFT = new List<MotionState>();
    public List<MotionState> motionStates_RIGHT = new List<MotionState>();

    Hashtable hashTable_IDLE = new Hashtable();
    Hashtable hashTable_FORWARD = new Hashtable();
    Hashtable hashTable_LEFT = new Hashtable();
    Hashtable hashTable_RIGHT = new Hashtable();

    public Rigidbody player_rb;
    public SensorControl sensor;
    public Animator anim;
    public Transform hips;

    public float rotationThreshold = 0.01f;
    public float idleThreshold = 0.1f;

    #region Serializeable Vector3 and Quaternions
    [System.Serializable]
    public struct SerializableVector3 {
        public float x, y, z;

        public SerializableVector3(float rX, float rY, float rZ) {
            x = rX;
            y = rY;
            z = rZ;
        }

        public override string ToString() {
            return string.Format("[{0}, {1}, {2}]", x, y, z);
        }

        public static implicit operator Vector3(SerializableVector3 rValue) {
            return new Vector3(rValue.x, rValue.y, rValue.z);
        }

        public static implicit operator SerializableVector3(Vector3 rValue) {
            return new SerializableVector3(rValue.x, rValue.y, rValue.z);
        }
    }

    [System.Serializable]
    public struct SerializableQuaternion {
        public float x, y, z, w;

        public SerializableQuaternion(float rX, float rY, float rZ, float rW) {
            x = rX;
            y = rY;
            z = rZ;
            w = rW;
        }

        public override string ToString() {
            return string.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
        }

        public static implicit operator Quaternion(SerializableQuaternion rValue) {
            return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
        }

        public static implicit operator SerializableQuaternion(Quaternion rValue) {
            return new SerializableQuaternion(rValue.x, rValue.y, rValue.z, rValue.w);
        }
    }

    sealed class Vector3SerializationSurrogate : ISerializationSurrogate {
        //Method called to serialize a Vector3 object
        public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context) {
            Vector3 v3 = (Vector3)obj;
            info.AddValue("x", v3.x);
            info.AddValue("y", v3.y);
            info.AddValue("z", v3.z);
            Debug.Log(v3);
        }

        //Method called to deserialize a Vector3 object
        public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector) {
            Vector3 v3 = (Vector3)obj;
            v3.x = (float)info.GetValue("x", typeof(float));
            v3.y = (float)info.GetValue("y", typeof(float));
            v3.z = (float)info.GetValue("z", typeof(float));
            obj = v3;
            return obj;
        }
    }

    sealed class QuaternionSerializationSurrogate : ISerializationSurrogate {
        //Method called to serialize a Quaternion object
        public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context) {
            Quaternion q = (Quaternion)obj;
            info.AddValue("x", q.x);
            info.AddValue("y", q.y);
            info.AddValue("z", q.z);
            info.AddValue("w", q.w);
            Debug.Log(q);
        }

        //Method called to deserialize a Quaternion object
        public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector) {
            Quaternion q = (Quaternion)obj;
            q.x = (float)info.GetValue("x", typeof(float));
            q.y = (float)info.GetValue("y", typeof(float));
            q.z = (float)info.GetValue("z", typeof(float));
            q.w = (float)info.GetValue("w", typeof(float));
            obj = q;
            return obj;
        }
    }
    #endregion

    #region Custom structs
    public struct MotionState {
        public char anim_id;
        public int frame;
        public Pose pose;
        public Velocity velocity;        
    }

    public struct Pose {
        public Vector3 root_pos;
        public Quaternion root_rot;
        public Vector3[] joint_pos; //NEW
        public Quaternion[] joint_rots;
    }

    public struct Velocity {
        public Vector3 root_pos;
        public Quaternion root_rot;
        public Quaternion[] joint_rots;
    }
    #endregion

    private void Awake() {
        if (FindObjectsOfType<MotionStates>().Length > 1) {
            Destroy(this);
        }

        DontDestroyOnLoad(this);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (player_rb == null) {
            print("Finding new player prefab");
            GameObject player = GameObject.FindWithTag("Player");
            if (player != null) {
                player_rb = player.GetComponent<Rigidbody>();
                sensor = player.GetComponent<SensorControl>();
                anim = player.GetComponent<Animator>();
                hips = GameObject.FindWithTag("PlayerHips").transform;

                anim.Play("idle");
                anim.speed = 1;
            }
        }
    }

    private void Start() {
        #region Motionstate database generation
        if (generateDatabases) {
            //Creating the database and reading data from the database into the list of motion states
            CreateMotionDatabase("idle", "MotionDatabase_idle.txt");
            CreateMotionDatabase("forward", "MotionDatabase_forward.txt");
            CreateMotionDatabase("left", "MotionDatabase_left.txt");
            CreateMotionDatabase("right", "MotionDatabase_right.txt");
        }
        #endregion

        #region Reading motionstate databases
        //Reading motion states for the 'idle' animation
        string clip = "idle";
        string path = "MotionDatabase_" + clip + ".txt";
        motionStates_IDLE = ReadMotionStates(path);

        //Reading motion states for the 'forward' animation
        clip = "forward";
        path = "MotionDatabase_" + clip + ".txt";
        motionStates_FORWARD = ReadMotionStates(path);

        //Reading motion states for the 'left' animation
        clip = "left";
        path = "MotionDatabase_" + clip + ".txt";
        motionStates_LEFT = ReadMotionStates(path);

        //Reading motion states for the 'right' animation
        clip = "right";
        path = "MotionDatabase_" + clip + ".txt";
        motionStates_RIGHT = ReadMotionStates(path);

        //Creating list of all motion states
        motionStates_ALL.AddRange(motionStates_IDLE);
        motionStates_ALL.AddRange(motionStates_FORWARD);
        motionStates_ALL.AddRange(motionStates_LEFT);
        motionStates_ALL.AddRange(motionStates_RIGHT);

        //print("Motion state count: I" + motionStates_IDLE.Count + ", F" + motionStates_FORWARD.Count + ", L" + motionStates_LEFT.Count + ", R" + motionStates_RIGHT.Count + ", ALL" + motionStates_ALL.Count);
        #endregion

        #region Hashtable generation
        foreach (MotionState m in motionStates_ALL) {
            if (m.anim_id != 'i') {
                //calculate the dissimilarities between this and all motion states in the 'idle'-animation
                float[] dissimilarities = new float[motionStates_IDLE.Count];
                for (int i = 0; i < motionStates_IDLE.Count; i++) {
                    dissimilarities[i] = CalculateDissimilarity(m, motionStates_IDLE[i]);
                }

                //then find the state with the smallest dissimilarity
                int indexOfLeastDissimilarity = dissimilarities.Length;
                float smallestDissimilarity = Mathf.Infinity;
                for(int i = 0; i < dissimilarities.Length; i++) {
                    if (dissimilarities[i] < smallestDissimilarity) {
                        indexOfLeastDissimilarity = i;
                        smallestDissimilarity = dissimilarities[i];
                    }
                }

                //add this motion state and the motion state that was picked as being the most similar (=smallest dissimilarity) to the IDLE-hash
                hashTable_IDLE.Add(m, motionStates_IDLE[indexOfLeastDissimilarity]);
            }

            if (m.anim_id != 'f') {
                //calculate the dissimilarities between this and all motion states in the 'forward'-animation
                float[] dissimilarities = new float[motionStates_FORWARD.Count];
                for (int i = 0; i < motionStates_FORWARD.Count; i++) {
                    dissimilarities[i] = CalculateDissimilarity(m, motionStates_FORWARD[i]);
                }

                //then find the state with the smallest dissimilarity
                int indexOfLeastDissimilarity = dissimilarities.Length;
                float smallestDissimilarity = Mathf.Infinity;
                for (int i = 0; i < dissimilarities.Length; i++) {
                    if (dissimilarities[i] < smallestDissimilarity) {
                        indexOfLeastDissimilarity = i;
                        smallestDissimilarity = dissimilarities[i];
                    }
                }

                //add this motion state and the motion state that was picked as being the most similar (=smallest dissimilarity) to the FORWARD-hash
                hashTable_FORWARD.Add(m, motionStates_FORWARD[indexOfLeastDissimilarity]);
            }

            if (m.anim_id != 'l') {
                //calculate the dissimilarities between this and all motion states in the 'left'-animation
                float[] dissimilarities = new float[motionStates_LEFT.Count];
                for (int i = 0; i < motionStates_LEFT.Count; i++) {
                    dissimilarities[i] = CalculateDissimilarity(m, motionStates_LEFT[i]);
                }

                //then find the state with the smallest dissimilarity
                int indexOfLeastDissimilarity = dissimilarities.Length;
                float smallestDissimilarity = Mathf.Infinity;
                for (int i = 0; i < dissimilarities.Length; i++) {
                    if (dissimilarities[i] < smallestDissimilarity) {
                        indexOfLeastDissimilarity = i;
                        smallestDissimilarity = dissimilarities[i];
                    }
                }

                //add this motion state and the motion state that was picked as being the most similar (=smallest dissimilarity) to the LEFT-hash
                hashTable_LEFT.Add(m, motionStates_LEFT[indexOfLeastDissimilarity]);
            }

            if (m.anim_id != 'r') {
                //calculate the dissimilarities between this and all motion states in the 'right'-animation
                float[] dissimilarities = new float[motionStates_RIGHT.Count];
                for (int i = 0; i < motionStates_RIGHT.Count; i++) {
                    dissimilarities[i] = CalculateDissimilarity(m, motionStates_RIGHT[i]);
                }

                //then find the state with the smallest dissimilarity
                int indexOfLeastDissimilarity = dissimilarities.Length;
                float smallestDissimilarity = Mathf.Infinity;
                for (int i = 0; i < dissimilarities.Length; i++) {
                    if (dissimilarities[i] < smallestDissimilarity) {
                        indexOfLeastDissimilarity = i;
                        smallestDissimilarity = dissimilarities[i];
                    }
                }

                //add this motion state and the motion state that was picked as being the most similar (=smallest dissimilarity) to the RIGHT-hash
                hashTable_RIGHT.Add(m, motionStates_RIGHT[indexOfLeastDissimilarity]);
            }
        }
        #endregion
    }

    private void FixedUpdate() {
        if (player_rb != null) {
            if (player_rb.velocity.magnitude < idleThreshold)
                currentState = 'i';
            else if (Mathf.Abs(sensor.rotationDifference) < rotationThreshold)
                currentState = 'f';
            else if (sensor.rotationDifference > 0)
                currentState = 'l';
            else if (sensor.rotationDifference < 0)
                currentState = 'r';
            else
                currentState = 'i';

            if (currentState != previousState) { //now we switch to a different animation
                SwitchMotionState(currentState, previousState);
            }

            previousState = currentState;
        }
    }

    private void SwitchMotionState(char currentState, char previousState) {
        //Find out which frame we're currently at
        float normTime = anim.GetCurrentAnimatorStateInfo(0).normalizedTime % 1f;
        int currentFrame = (int)(normTime * GetTotalFramesForAnimation(previousState));

        if (currentFrame < 1) {
            currentFrame = 1;
        }

        //Find the corresponding frame in the walk-animation and play that
        MotionState similarState = (MotionState)GetHashtableForAnimation(currentState)[GetMotionStateListForAnimation(previousState)[currentFrame - 1]];
        anim.CrossFade(GetNameForAnimation(currentState), 0.1f, 0, (float)similarState.frame / GetTotalFramesForAnimation(currentState));

        print("Switched from " + GetNameForAnimation(previousState) + currentFrame + " to " + GetNameForAnimation(currentState) + similarState.frame);
    }

    #region Switch-functions for getting correct animation name/framecount/hashtable/motionstate-list
    private string GetNameForAnimation(char anim_id) {
        switch (anim_id) {
            case 'i':
                return "idle";
            case 'f':
                return "forward";
            case 'l':
                return "left";
            case 'r':
                return "right";
            default:
                Debug.LogError("Invalid anim_id in GetTotalFramesForAnimation(anim_id)");
                return "idle";
        }
    }

    private List<MotionState> GetMotionStateListForAnimation(char anim_id) {
        switch (anim_id) {
            case 'i':
                return motionStates_IDLE;
            case 'f':
                return motionStates_FORWARD;
            case 'l':
                return motionStates_LEFT;
            case 'r':
                return motionStates_RIGHT;
            default:
                Debug.LogError("Invalid anim_id in GetTotalFramesForAnimation(anim_id)");
                return motionStates_IDLE;
        }
    }

    private Hashtable GetHashtableForAnimation(char anim_id) {
        switch (anim_id) {
            case 'i':
                return hashTable_IDLE;
            case 'f':
                return hashTable_FORWARD;
            case 'l':
                return hashTable_LEFT;
            case 'r':
                return hashTable_RIGHT;
            default:
                Debug.LogError("Invalid anim_id in GetTotalFramesForAnimation(anim_id)");
                return hashTable_IDLE;
        }
    }

    private int GetTotalFramesForAnimation(char anim_id) {
        switch (anim_id) {
            case 'i':
                return totalFrames_idle;
            case 'f':
                return totalFrames_forward;
            case 'l':
                return totalFrames_left;
            case 'r':
                return totalFrames_right;
            default:
                Debug.LogError("Invalid anim_id in GetTotalFramesForAnimation(anim_id)");
                return totalFrames_idle;
        }
    }
    #endregion

    private float CalculateDissimilarity(MotionState current, MotionState candidate) {
        float result = Mathf.Infinity;

        float beta_root = 0.5f;
        float beta_zero = 0.5f;

        float piece_1 = beta_root * Mathf.Pow((current.velocity.root_pos - candidate.velocity.root_pos).magnitude, 2f);
        float piece_2 = beta_zero * Mathf.Pow((current.velocity.root_rot * Vector3.forward - candidate.velocity.root_rot * Vector3.forward).magnitude, 2f);
        float piece_3 = 0f;
        float piece_4 = 0f;


        for (int i = 0; i < current.pose.joint_rots.Length; i++) {
            float weight = 0f;
            try {
                weight = (current.pose.joint_pos[i] - current.pose.joint_pos[i + 1]).magnitude;
            }
            catch (System.IndexOutOfRangeException e) {
                
            }

            piece_3 += weight * Mathf.Pow((current.pose.joint_rots[i] * Vector3.forward - candidate.pose.joint_rots[i] * Vector3.forward).magnitude, 2f);
            piece_4 += weight * Mathf.Pow((((current.velocity.joint_rots[i] * current.pose.joint_rots[i]) * Vector3.forward) - ((candidate.velocity.joint_rots[i] * candidate.pose.joint_rots[i]) * Vector3.forward)).magnitude, 2f);
        }

        result = Mathf.Sqrt(piece_1 + piece_2 + piece_3 + piece_4);

        return result;
    }

    #region Generating motionstates and database
    private void CreateMotionDatabase(string clip, string fileName) {
        List<MotionState> motionStates = new List<MotionState>();

        //Finding out the amount of frames in the current clip
        anim.Play(clip);
        anim.speed = 1;
        anim.Update(1);
        float clipLength = anim.GetCurrentAnimatorStateInfo(0).length;
        int totalFrames = (int)(clipLength * _framerate);
        anim.speed = 0;

        char anim_id = clip[0];

        for (int frame = 1; frame <= totalFrames; frame++) {
            MotionState m = new MotionState();
            Pose p = ConstructPose(clip, frame, totalFrames);
            Velocity v = new Velocity();

            m.anim_id = anim_id;
            m.frame = frame;

            //CONSTRUCTING THE VELOCITY V
            Pose p1 = ConstructPose(clip, frame + 1, totalFrames); //NOTE: This could be problematic when we reach the last frame (because there's no "frame + 1")
            v.root_pos = p1.root_pos - p.root_pos;
            v.root_rot = p1.root_rot * Quaternion.Inverse(p.root_rot);
            v.joint_rots = new Quaternion[p.joint_rots.Length];
            
            for (int i = 0; i < v.joint_rots.Length; i++) {
                v.joint_rots[i] = p1.joint_rots[i] * Quaternion.Inverse(p.joint_rots[i]);
            }

            //SAVING THE MOTION STATE
            m.pose = p;
            m.velocity = v;

            motionStates.Add(m);
        }

        FormatMotionStates(motionStates, fileName);
    }

    private void FormatMotionStates(List<MotionState> mstates, string fileName) {
        Debug.Log("Formatting...");

        string path = Application.persistentDataPath + "/" + fileName;
        if (File.Exists(path)) //delete file if it already exists
            File.Delete(path);
        FileStream fs = File.Create(path);

        BinaryFormatter bf = new BinaryFormatter();

        //1. Construct a SurrogateSelector object
        SurrogateSelector ss = new SurrogateSelector();
        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3ss);

        //2. Have the formatter use our surrogate selector
        bf.SurrogateSelector = ss;

        //Break the motion state down to be serialized
        foreach (MotionState m in mstates) {
            bf.Serialize(fs, m.anim_id);
            bf.Serialize(fs, m.frame);

            SerializableVector3 p_root_pos = (SerializableVector3)m.pose.root_pos;
            SerializableQuaternion p_root_rot = (SerializableQuaternion)m.pose.root_rot;
            bf.Serialize(fs, p_root_pos);
            bf.Serialize(fs, p_root_rot);

            //NEW LOOP
            foreach (Vector3 v in m.pose.joint_pos) {
                SerializableVector3 p_joint_pos = (SerializableVector3)v;
                bf.Serialize(fs, p_joint_pos);
            }

            foreach (Quaternion q in m.pose.joint_rots) {
                SerializableQuaternion p_joint_rots = (SerializableQuaternion)q;
                bf.Serialize(fs, p_joint_rots);
            }

            SerializableVector3 v_root_pos = (SerializableVector3)m.velocity.root_pos;
            SerializableQuaternion v_root_rot = (SerializableQuaternion)m.velocity.root_rot;
            bf.Serialize(fs, v_root_pos);
            bf.Serialize(fs, v_root_rot);

            foreach (Quaternion q in m.velocity.joint_rots) {
                SerializableQuaternion v_joint_rots = (SerializableQuaternion)q;
                bf.Serialize(fs, v_joint_rots);
            }
        }

        fs.Close();

        //Debug.Log("Finished formatting.");
    }

    private List<MotionState> ReadMotionStates(string fileName) {
        Debug.Log("Reading motion state data from file...");
        List<MotionState> deserializedStates = new List<MotionState>();

        string path = Application.persistentDataPath + "/" + fileName; //FOR PC VERSION

        if (File.Exists(path)) {
            BinaryFormatter bf = new BinaryFormatter();

            //1. Construct a SurrogateSelector object
            SurrogateSelector ss = new SurrogateSelector();
            Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
            ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), v3ss);

            //2. Have the formatter use our surrogate selector
            bf.SurrogateSelector = ss;

            FileStream fs = File.Open(path, FileMode.Open);

            //Recreate the motion states by deserialization
            while (fs.Position < fs.Length) {
                MotionState m = new MotionState();
                Pose p = new Pose();
                Velocity v = new Velocity();

                m.anim_id = (char)bf.Deserialize(fs);
                m.frame = (int)bf.Deserialize(fs);

                p.root_pos = (Vector3)((SerializableVector3)bf.Deserialize(fs));
                p.root_rot = (Quaternion)((SerializableQuaternion)bf.Deserialize(fs));
                p.joint_pos = new Vector3[_boneCount]; //NEW
                p.joint_rots = new Quaternion[_boneCount];

                //NEW LOOP
                for (int i = 0; i < _boneCount; i++) {
                    p.joint_pos[i] = (Vector3)((SerializableVector3)bf.Deserialize(fs));
                }

                for (int i = 0; i < _boneCount; i++) {
                    p.joint_rots[i] = (Quaternion)((SerializableQuaternion)bf.Deserialize(fs));
                }

                v.root_pos = (Vector3)((SerializableVector3)bf.Deserialize(fs));
                v.root_rot = (Quaternion)((SerializableQuaternion)bf.Deserialize(fs));
                v.joint_rots = new Quaternion[_boneCount];

                for (int i = 0; i < _boneCount; i++) {
                    v.joint_rots[i] = (Quaternion)((SerializableQuaternion)bf.Deserialize(fs));
                }

                m.pose = p;
                m.velocity = v;

                deserializedStates.Add(m);
            }

            fs.Close();
        }

       // Debug.Log("Finished reading.");

        return deserializedStates;
    }

    private Pose ConstructPose(string clip, int frame, int totalFrames) {
        Pose p = new Pose();

        //Updating the rig positions and rotations to the new frame
        anim.Play(clip, 0, (float)(frame) / (float)(totalFrames));
        anim.Update(0);
        anim.speed = 0;

        //Saving root (hip) position + rotation
        //p.root_pos = anim.GetBoneTransform(HumanBodyBones.Hips).position;
        //p.root_rot = anim.GetBoneTransform(HumanBodyBones.Hips).rotation;

        p.root_pos = hips.position;
        p.root_rot = hips.rotation;

        //print(clip + ":" + hips.position.ToString("F5") + " in frame " + frame + ", time " + (float)(frame) / (float)(totalFrames));

        //Getting joint rotations (all children of the hip-bone)
        //Transform[] transforms = anim.GetBoneTransform(HumanBodyBones.Hips).gameObject.GetComponentsInChildren<Transform>();
        Transform[] transforms = hips.gameObject.GetComponentsInChildren<Transform>();
        List<Vector3> positions = new List<Vector3>(); //NEW
        List<Quaternion> rotations = new List<Quaternion>();
        foreach (Transform t in transforms) {
            positions.Add(t.localPosition); //NEW
            rotations.Add(t.rotation);
        }
        positions.RemoveAt(0); //NEW
        rotations.RemoveAt(0);

        p.joint_pos = positions.ToArray(); //NEW
        p.joint_rots = rotations.ToArray();

        return p;
    }
    #endregion
}