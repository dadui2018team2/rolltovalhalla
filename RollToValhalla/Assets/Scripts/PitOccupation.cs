﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitOccupation : MonoBehaviour {

	private bool occupied = false;

	private void Start() {
		occupied = false;
	}

	private void OnEnable() {
		occupied = false;
	}

	public void BecomeOccupied(){
		occupied = true;
	}

	public bool IsOccupied{
		get {return occupied;}
	}
}
