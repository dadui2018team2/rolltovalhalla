﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitBehaviour : MonoBehaviour {

    bool moveBoulder = false;
    public bool boulderMoved = false;
    public float triggerDistance = 1f;
    public float distanceToFall = 1f;

    GameObject pitDetector;

    Quaternion originalRotation;

    private Rigidbody rb;
    private PitOccupation po;

    private bool canPlayAudio = true;

	// Use this for initialization
	void Start () {
        originalRotation = transform.rotation;
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (moveBoulder)
        {
            float distanceToBoulder = Vector3.Distance(pitDetector.transform.position, gameObject.transform.position);

            if (distanceToBoulder < triggerDistance && !boulderMoved)
            {
                rb.useGravity = true;
                rb.constraints = RigidbodyConstraints.FreezeRotation;
                gameObject.GetComponent<BoxCollider>().enabled = false;

                //boulderMoved = true;
                float speed = rb.velocity.magnitude;

                transform.rotation = Quaternion.Slerp(transform.rotation, originalRotation, speed * 20 * Time.deltaTime);

                Vector3 boulderPosition = new Vector3(pitDetector.transform.position.x, pitDetector.transform.position.y - 0.2f, pitDetector.transform.position.z);
                rb.velocity = Vector3.zero;

                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, boulderPosition, (speed*10) * Time.deltaTime);

                gameObject.GetComponent<SensorControl>().enabled = false;

                if(distanceToBoulder <= 0.2f)
                {
                    boulderMoved = true;
                    moveBoulder = false;
                }
            }
        }

        if (boulderMoved)
        {
            if (Vector3.Distance(pitDetector.transform.position, gameObject.transform.position) > distanceToFall)
            {
                gameObject.GetComponent<BoxCollider>().enabled = true;
                gameObject.GetComponent<Rigidbody>().isKinematic = true;
                if (canPlayAudio)
                {
                    AkSoundEngine.PostEvent("BoulderFall", gameObject);
                    canPlayAudio = false;
                }
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Pit")
        {
            po = other.GetComponent<PitOccupation>();
            if (!po.IsOccupied){
                moveBoulder = true;
                pitDetector = other.gameObject;
                po.BecomeOccupied();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Pit")
        {
            moveBoulder = false;
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
