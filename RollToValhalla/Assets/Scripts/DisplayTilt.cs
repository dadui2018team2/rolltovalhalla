﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTilt : MonoBehaviour {

	public Text text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 androidInput = new Vector3(Input.acceleration.x,0,Input.acceleration.y);
		text.text = androidInput.ToString() + "\n" + GameManager.LevelInfo;
	}
}
