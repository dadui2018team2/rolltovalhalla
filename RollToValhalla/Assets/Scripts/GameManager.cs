﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //The strings used for saving items to player preferences.
    private static string levelUnlockKey = "unlocked_levels";
    private static string musicVolumeKey = "volume_music";
    private static string sfxVolumeKey = "volume_sfx";

	[Tooltip("The current level number.")]
	public int _thisLevel = 0;
	private static int thisLevel;
	public Room[] _rooms;

	private static GameManager instance = null;
	private static GameObject player;
	private static Room[] rooms;
	private static int currentRoom = 0;

	private static Vector3 cameraOffset;

	private void Awake() {
		if (!instance) instance = this;
		else Destroy(this);

		player = GameObject.FindWithTag("Player");

        currentRoom = 0;
		rooms = _rooms;
		for (int i = 0; i < rooms.Length; i++){
			rooms[i].RoomNumber = i;
			rooms[i].gameObject.SetActive(false);
		} 
		
		cameraOffset = Camera.main.transform.position - rooms[0].transform.position;
        //print(rooms[currentRoom]);
		thisLevel = _thisLevel;
	}

	private void Start() {
		SpawnPlayer();
	}

	private static void SpawnPlayer(){
		rooms[currentRoom].gameObject.SetActive(true);
		rooms[currentRoom].ResetRoom();
		player.transform.position = rooms[currentRoom].spawnPoint.transform.position;
		player.GetComponent<Rigidbody>().velocity = Vector3.zero;
		MoveCamera();
	}

	public static void NextRoom(){
		rooms[currentRoom].gameObject.SetActive(false);
		if (currentRoom < rooms.Length - 1){
			currentRoom++;
			SpawnPlayer();
		}
		else{
			//Complete level

			SaveLatestLevel();
			SceneManager.LoadScene("LevelSelection");
			/*currentRoom = 0;
			SpawnPlayer();*/
		}
	}

	public static void PlayerDeath(){
        rooms[currentRoom].gameObject.SetActive(false);
		while (currentRoom > 0 && !rooms[currentRoom].isCheckpoint) currentRoom--;
		SpawnPlayer();
        AkSoundEngine.PostEvent("ManGetsKilled_Slow", GameObject.FindGameObjectWithTag("Player"));
	}

    public void GoToRoom(int roomNumber) {
        rooms[currentRoom].gameObject.SetActive(false);
        currentRoom = roomNumber;
        SpawnPlayer();
    }

	private static void MoveCamera(){
		Camera.main.transform.position = rooms[currentRoom].transform.position + cameraOffset;
	}

	public static string LevelInfo{
		get{return "Level " + thisLevel + " - Room " + currentRoom;}
	}

    #region SaveAndLoad
    static public void SaveLatestLevel() {
		if (thisLevel > PlayerPrefs.GetInt(levelUnlockKey, 0)){
			PlayerPrefs.SetInt(levelUnlockKey, thisLevel);
		}
    }

    public void SaveSoundSettings(float musicVolume, float sfxVolume) {
        PlayerPrefs.SetFloat(musicVolumeKey, musicVolume);
        PlayerPrefs.SetFloat(sfxVolumeKey, sfxVolume);
    }
    #endregion
}
