﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorControl : MonoBehaviour {

	/*
	This is the SensorControl script
	Used for objects that can move by gyro-controls
	Placed on player and boulder.
	Give player "Player" tag, and boulder "Boulder" tag.
	Give player sub-10 values, while boulder should probably have at least 10 acceleration
	Alse, give boulder the boulder physics material so we can avoid the friction created by the box collider
	*/

	public float maxSpeed = 1.0f;
	public float minSpeed = 0.01f;
	public float acceleration = 1.0f;
	public float deceleration = 1.0f;
	
	public bool canTurn = false;
	public float turnDegreeDelta = 90.0f;

	[Tooltip("Minimum degrees needed to tilt.")]
	public float minDegree = 0.05f;
	[Tooltip("Degrees needed to reach max tilt.")]
	public float maxDegree = 0.5f;

	private Rigidbody rb;
	private Vector3 calibrationVector;

    public float [] initialIdleTimer;
    public float currentIdleTimer;

    public float rotationDifference; //THIS IS USED FOR MOTION MATCHING

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		Calibrate();

        if (gameObject.tag == "Boulder")
        {
            AkSoundEngine.PostEvent("Boulder_Rolling_Loop", gameObject);
            //AkSoundEngine.SetRTPCValue("Boulder_Rolling_Loop", 0, gameObject);
        }
        else if (gameObject.tag == "Player")
        {
            currentIdleTimer = Random.Range(initialIdleTimer[0], initialIdleTimer[initialIdleTimer.Length-1]);
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 moveInput = Vector3.zero;
		
		Vector3 androidInput = new Vector3(Input.acceleration.x,0,Input.acceleration.y);
		Vector3 keyboardInput = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		// Use either android or keyboard input, whichever with the highest magnitude
		if (androidInput.magnitude > keyboardInput.magnitude) moveInput = tiltControl(androidInput);
		else moveInput = keyboardInput;

		// Limit input magnitude to max 1.0f
		if (moveInput.magnitude > 1) moveInput = moveInput.normalized;

		moveInput *= acceleration;
		// Add as velocity change
		rb.AddForce((moveInput + (rb.velocity.normalized * Mathf.Min(rb.velocity.magnitude, 1)) * -deceleration) * Time.deltaTime, ForceMode.VelocityChange);
		
		// Limiting the velocity to the max value
		if (rb.velocity.magnitude > maxSpeed){
			rb.velocity = rb.velocity.normalized * maxSpeed;
		}
		else if (rb.velocity.magnitude < minSpeed) {
			rb.velocity = Vector3.zero;
		}
		
		// Turning the object/player if we desire
		if (canTurn && rb.velocity.magnitude > minSpeed){
			Quaternion targetRotation = Quaternion.Euler(Vector3.up * (Mathf.Atan2(rb.velocity.x, rb.velocity.z) / Mathf.PI) * 180.0f);
            rotationDifference = Quaternion.ToEulerAngles(transform.rotation).y - Quaternion.ToEulerAngles(targetRotation).y;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnDegreeDelta);
        }

        if (gameObject.tag == "Boulder")
        {
            AkSoundEngine.SetRTPCValue("Boulder_Velocity", ((rb.velocity.magnitude / maxSpeed) * 100), gameObject);
        }
        else if (gameObject.tag == "Player")
        {
            PlayIdlePlayerSound();
        }
	}

	private Vector3 tiltControl(Vector3 input){
		float newMagnitude = Mathf.Min(Mathf.Max(input.magnitude - minDegree, 0) * (1/maxDegree-minDegree), 1);
		return input.normalized * newMagnitude;
	}

	private void Calibrate(){
		calibrationVector = new Vector3(Input.acceleration.x,0,Input.acceleration.y);
	}

    private void PlayIdlePlayerSound()
    {
        currentIdleTimer -= Time.deltaTime;

        if (currentIdleTimer <= 0)
        {
            AkSoundEngine.PostEvent("MainVoice_Idle", gameObject);
            currentIdleTimer = Random.Range(initialIdleTimer[0], initialIdleTimer[initialIdleTimer.Length-1]);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        string collisionName = collision.gameObject.name;
        if (collision.gameObject.tag == "Audio_Surface" && gameObject.tag == "Player")
        {
            if (collisionName.Contains("Fence") || collisionName.Contains("Tree"))
            {
                AkSoundEngine.PostEvent("ManHitsSurface_Tree", gameObject);
            }
            else if (collisionName.Contains("ThickWall") || collisionName.Contains("Wall"))
            {
                AkSoundEngine.PostEvent("ManHitsSurface_Wall", gameObject);
            }
        }
        else if (collision.gameObject.tag == "Audio_Surface" && gameObject.tag == "Boulder")
        {
            AkSoundEngine.PostEvent("BoulderHitsTree", gameObject);
        }
    }
}
