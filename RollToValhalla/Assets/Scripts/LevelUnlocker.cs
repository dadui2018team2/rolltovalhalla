﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUnlocker : MonoBehaviour {

	public GameObject[] levelButtons;

	private string levelUnlockKey = "unlocked_levels";
	private int currentLevel = 0;
	private bool locked = true;

	// Use this for initialization
	void Start () {
		currentLevel = PlayerPrefs.GetInt(levelUnlockKey, 0);
		SetLevelButtons();
	}

	private void SetLevelButtons(){
		for (int i = 0; i < levelButtons.Length; i++){
			if (i <= currentLevel){
				levelButtons[i].SetActive(true);
			}
			else {
				levelButtons[i].SetActive(false);
			}
		}
	}

	public void SetUnlockedLevels(bool setLock){
		locked = setLock;

		if (locked){
			currentLevel = PlayerPrefs.GetInt(levelUnlockKey, 0);
			if (currentLevel >= 5){
				PlayerPrefs.SetInt(levelUnlockKey, 0);
				currentLevel = 0;
			}
			SetLevelButtons();
		}
		else {
			currentLevel = 5;
			SetLevelButtons();
		}
	}

	public void ResetProgress(){
		PlayerPrefs.SetInt(levelUnlockKey, 0);
		SetLevelButtons();
	}
}
