﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSettings : MonoBehaviour {
	/*
	This script is for changing the shown language in the game.
	Add to text and then give both english and danish text.

	Based on an int:
		0 = English
		1 = Dansih
	
	 */

	public string english;
	public string danish;

	private string langKey = "lang";
	private int currentLang;

	private Text textComponent;

	// Use this for initialization
	void Start () {
		textComponent = GetComponent<Text>();
		UpdateThisText();
	}

	public void ChangeLanguage(){
		if (currentLang == 0){
			PlayerPrefs.SetInt(langKey, 1);
		}
		else {
			PlayerPrefs.SetInt(langKey, 0);
		}
		LanguageObjectsUpdate();
	}

	private void LanguageObjectsUpdate(){
		LanguageSettings[] lsArray = FindObjectsOfType<LanguageSettings>();
		foreach (LanguageSettings ls in lsArray){
			ls.UpdateThisText();
		}
	}

	public void UpdateThisText(){
		currentLang = PlayerPrefs.GetInt(langKey, 0);
		if (textComponent != null){
			if (currentLang == 0){
				textComponent.text = english;
			}
			else {
				textComponent.text = danish;
			}
		}
		
	}
}
