﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

	public bool isCheckpoint = false;
	public GameObject spawnPoint;

	[SerializeField]
	private int roomNumber;
	private SeeingEnemy[] valkyries;
	private SensorControl[] boulders;

	struct StartPosition {
		public Vector3 pos;
		public Quaternion rot;
	}

	private StartPosition[] startValkyries;
	private StartPosition[] startBoulders;

	// Use this for initialization
	void Awake () {
		// Get boulders
		boulders = GetComponentsInChildren<SensorControl>();
		startBoulders = new StartPosition[boulders.Length];
		if (boulders.Length > 0){
			for (int i = 0; i < boulders.Length; i++){
				startBoulders[i].pos = boulders[i].transform.position;
				startBoulders[i].rot = boulders[i].transform.rotation;
			}
		}

		// Get valkyries
		valkyries = GetComponentsInChildren<SeeingEnemy>();
		startValkyries = new StartPosition[valkyries.Length];
		if (valkyries.Length > 0){
			for (int i = 0; i < valkyries.Length; i++){
				startValkyries[i].pos = valkyries[i].transform.position;
				startValkyries[i].rot = valkyries[i].transform.rotation;
			}
		}
	}
	
	public void ResetRoom(){
		if (boulders.Length > 0){
			for (int i = 0; i < boulders.Length; i++){
				boulders[i].transform.position = startBoulders[i].pos;
				boulders[i].transform.rotation = startBoulders[i].rot;
				Rigidbody rb = boulders[i].GetComponent<Rigidbody>();
				rb.velocity = Vector3.zero;
				rb.useGravity = false;
				rb.isKinematic = false;
				rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
				boulders[i].enabled = true;
				boulders[i].GetComponent<PitBehaviour>().boulderMoved = false;
			}
		}
		
		if (valkyries.Length > 0){
			for (int i = 0; i < valkyries.Length; i++){
				valkyries[i].transform.position = startValkyries[i].pos;
				valkyries[i].transform.rotation = startValkyries[i].rot;

				valkyries[i].gameObject.SetActive(false);
				valkyries[i].gameObject.SetActive(true);
			}
		}
		
	}

	public Vector3 getSpawnPoint{
		get {return spawnPoint.transform.position;}
	}

	public int RoomNumber{
		get {return roomNumber;}
		set {roomNumber = value;}
	}
}
