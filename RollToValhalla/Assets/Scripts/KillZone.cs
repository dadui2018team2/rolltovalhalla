﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour {

	public bool killValkyrie = false;

	private void OnTriggerEnter(Collider other) {
		kill(other.gameObject);
    }

	private void OnCollisionEnter(Collision other) {
		kill(other.gameObject);
	}

	private void kill(GameObject other){
		if(other.tag.Equals("Player")) GameManager.PlayerDeath();
		if (killValkyrie){
			if(other.tag.Equals("Valkyrie")) other.gameObject.SetActive(false);
		}
	}
}
