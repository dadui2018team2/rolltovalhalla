﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeeingEnemy : MonoBehaviour {

    /*
    A script for enabling an enemy to react to the player entering their field of view.
    It requires a child object with a trigger collider and a FieldOfView.cs (to be set as the 'viewCone').

    New reactions can be added later (in the TriggerEvent) if needed.
    */

    [SerializeField] GameObject player;
    [SerializeField] FieldOfView viewCone1;
    [SerializeField] FieldOfView viewCone2;
    [SerializeField] NavMeshAgent navAgent;

    bool chasePlayer = false; //This variable will only be in use if the enemy should be able to chase the player.

    //Used by enraged valkyries to return to their original positions if no entity is nearby.
    public bool returningToInit = false;
    public Vector3 initPosition;
    public Quaternion initRotation;
    public float maxDegreesDelta = 3f;

    //Used to determine audio upon InstaDeath. Set to either "Fast" or "Slow" in inspector depending on valkyrie type.
    public string valkyrieType;

    public bool playSound = true;
    

    public TriggerEvent trigger = TriggerEvent.Chase;
    public enum TriggerEvent
    {
        Chase,
        InstantDeath,
        Enrage
    }

    public Animator anim;

    private void Awake() {
        player = GameObject.FindWithTag("Player");
    }

    private void Start() {
        initPosition = transform.position;
        initRotation = transform.rotation;
        
    }

    private void OnEnable() {
        chasePlayer = false;
        ValkyriePatrolling patrolScript = GetComponent<ValkyriePatrolling>();
                if (patrolScript != null)
                    patrolScript.enabled = true;
    }

    private void Update() {
        if (anim != null && navAgent != null && trigger != TriggerEvent.InstantDeath) anim.SetFloat("Speed", navAgent.velocity.magnitude);

        //If we should be chasing the player, we set our NavAgent-destination. No need to check if the player is in the field of view then.
        if (chasePlayer) {
            navAgent.SetDestination(player.transform.position);
            return;
        }

        if (trigger == TriggerEvent.Enrage) {
            if (viewCone1.preyInView.Count > 0 || viewCone2.preyInView.Count > 0) {
                ChaseNearest();
                if (playSound)
                {
                    AkSoundEngine.PostEvent("ValkyrieSlow_Voice", gameObject);
                    playSound = false;
                }
            }
            else {
                ReturnToInit();
            }
            return;
        }

        //If the player is within our field of view, we check to see whether any obstacles block our view of the player. If not, we call ReactToPlayer().
        if (viewCone1.playerInView || viewCone2.playerInView) {
            RaycastHit rayHit;
            if (Physics.Raycast(transform.position, player.transform.position - transform.position, out rayHit)) {
                if (rayHit.collider.tag == "Player")
                    ReactToPlayer();
            }

        }

        
    }

    private void ChaseNearest() {
        returningToInit = false;
        List<GameObject> nearbyPrey = viewCone1.preyInView;

        float smallestDistance = Mathf.Infinity;
        GameObject nearest = null;

        foreach (GameObject obj in nearbyPrey) {
            float distance = (transform.position - obj.transform.position).magnitude;
            if (distance < smallestDistance) {
                smallestDistance = distance;
                nearest = obj;
            }
        }

        if (nearest != null) {
            navAgent.SetDestination(nearest.transform.position);
        }
    }

    private void ReturnToInit() {
        if (!returningToInit && transform.position != initPosition) {
            returningToInit = true;
            navAgent.SetDestination(initPosition);
        }

        if ((transform.position - initPosition).magnitude < 0.4f) {
            returningToInit = false;
            if (transform.rotation != initRotation) {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, initRotation, maxDegreesDelta);
            }
        }
        playSound = true;
    }

    private void ReactToPlayer() {
        switch (trigger) {
            case TriggerEvent.Chase:
                chasePlayer = true;
                ValkyriePatrolling patrolScript = GetComponent<ValkyriePatrolling>();
                if (patrolScript != null)
                    patrolScript.enabled = false;
                AkSoundEngine.PostEvent("ValkyrieFast_Voice", gameObject);
                break;
            case TriggerEvent.InstantDeath:
                GameManager.PlayerDeath();
                break;
            default:
                break;
        }
    }
}
