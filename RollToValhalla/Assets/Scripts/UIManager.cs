﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public GameObject menu;
    public Slider musicSlider;
    public Slider sfxSlider;

    public float musicVolume;
    public float sfxVolume;


    private void Awake() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (musicSlider.value != musicVolume) {
            musicSlider.value = musicVolume;
        }
        if (sfxSlider.value != sfxVolume) {
            sfxSlider.value = sfxVolume;
        }
    }

    public void GoToScene(string sceneName) {
        PlaySound();
        SceneManager.LoadScene(sceneName);
    }

    public void OpenMenu() {
        PlaySound();
        menu.SetActive(true);
    }

    public void CloseMenu() {
        PlaySound();
        menu.SetActive(false);
    }

    public void ChangeMusicVolume() {
        musicVolume = musicSlider.value * 100;
        AkSoundEngine.SetRTPCValue("Music_Volume", musicVolume);
    }

    public void ChangeSFXVolume() {
        sfxVolume = sfxSlider.value * 100;
        AkSoundEngine.SetRTPCValue("Sound_Volume", sfxVolume);
    }

    private void PlaySound() {
        AkSoundEngine.PostEvent("UI_Click", gameObject);
    }
}
