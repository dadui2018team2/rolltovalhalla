﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour {

    /*
    A script for registering when the player enters this collider.
    Should be put on an object with a trigger collider (which will then represent 'the field of vision').
    */

    public bool playerInView = false;
    public List<GameObject> preyInView = new List<GameObject>();

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            playerInView = true;
            preyInView.Add(other.gameObject);
        }
        else if (other.tag == "Valkyrie") {
            preyInView.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player")
            playerInView = false;

        if (other.tag == "Player" || other.tag == "Valkyrie") {
            if (preyInView.Contains(other.gameObject))
                preyInView.Remove(other.gameObject);
        }
    }

    private void OnDisable() {
        playerInView = false;
        preyInView.Clear();
    }
}
