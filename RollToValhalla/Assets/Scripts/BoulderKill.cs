﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderKill : MonoBehaviour {

	public float minVelPlayer = 1.0f;
	public float minVelValkyrie = 1.0f;

	private Rigidbody rb;

	private void Start() {
		rb = GetComponent<Rigidbody>();
	}

	private void OnCollisionEnter(Collision other) {
		if (other.gameObject.tag.Equals("Player")){
			float playerV = other.gameObject.GetComponent<Rigidbody>().velocity.magnitude;
			float boulderV = rb.velocity.magnitude;
			float relativeV = other.relativeVelocity.magnitude;
			if (boulderV >= minVelPlayer && relativeV > playerV && boulderV > playerV){
				GameManager.PlayerDeath();
			}
		}
		else if (other.gameObject.tag.Equals("Valkyrie")){
			SeeingEnemy se = other.gameObject.GetComponent<SeeingEnemy>();
			if (se.trigger == 0){
				float valkyrieV = other.gameObject.GetComponent<Rigidbody>().velocity.magnitude;
				float boulderV = rb.velocity.magnitude;
				float relativeV = other.relativeVelocity.magnitude;
				if (boulderV >= minVelPlayer && relativeV > valkyrieV && boulderV > valkyrieV){
					other.gameObject.SetActive(false);
				}
			}
		}
	}
}
