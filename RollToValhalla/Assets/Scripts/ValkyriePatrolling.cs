﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValkyriePatrolling : MonoBehaviour {

    [System.Serializable]
    public struct PatrolPoint {
        public Vector3 position;
        public Vector3 rotation;
    }

    public float timeBetweenMoves = 1f;
    public List<PatrolPoint> patrolRoute = new List<PatrolPoint>();

    float timeSinceLastMovement = 0f;
    bool isMoving = false;

    public float maxDegreesDelta;
    public float maxDistanceDelta;
	
    private List<PatrolPoint> initialPatrolRoute;
    private Animator anim; 
    private Vector3 lastPos;

    private void Awake() {
        initialPatrolRoute = patrolRoute;
    }
    
    private void OnEnable() {
        patrolRoute = initialPatrolRoute;
        anim = GetComponentInChildren<Animator>();
        lastPos = transform.position;
    }

	// Update is called once per frame
	void Update () {

        if (anim != null){
            Vector3 vel = transform.position - lastPos;
            anim.SetFloat("Speed", vel.magnitude / Time.deltaTime);
            lastPos = transform.position;
        } 

        if (patrolRoute.Count < 1)
            return;

        if (!isMoving) //we should not currently be moving, so we just increase our timer for next movement
        {
            timeSinceLastMovement += Time.deltaTime;
        }
        else //we should be moving, so call the Move()-function
            Move();

        if (timeSinceLastMovement >= timeBetweenMoves) { //if our timer has reached a point where we should move, we start moving
            isMoving = true;
            timeSinceLastMovement = 0f;
        }
	}

    private void Move() {
        Quaternion lookTarget = Quaternion.Euler(patrolRoute[0].rotation);

        if (transform.rotation != lookTarget) { //if not properly rotated, rotate
            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookTarget, maxDegreesDelta);
        }
        else if (transform.position != patrolRoute[0].position) { //we are properly rotated, so if not properly placed, start moving
            transform.position = Vector3.MoveTowards(transform.position, patrolRoute[0].position, maxDistanceDelta);
        }
        else { //move the patrol point we've just reached to the back of the list
            PatrolPoint point = patrolRoute[0];
            patrolRoute.RemoveAt(0);
            patrolRoute.Add(point);
            isMoving = false;
        }
    }
}
