﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomExit : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		if (other.tag.Equals("Player")){
			GameManager.NextRoom();

            string scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            scene = scene.Replace("_v2", "");
            scene = scene.Replace("_v3", "");
            scene = scene.Replace("_v4", "");
            char sceneNumber = scene[scene.Length-1];

            switch (sceneNumber) {
                case '1': case '4': case '5':
                    AkSoundEngine.PostEvent("PrisonDoor", gameObject);
                    break;
                case '2': case '3':
                    AkSoundEngine.PostEvent("BranchDoor", gameObject);
                    break;
                default:
                    break;
            }
		}
	}
}
