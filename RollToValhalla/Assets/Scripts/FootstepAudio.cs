﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepAudio : MonoBehaviour {

    public string soundName;

    public float initialCounter;
    public float currentCounter;

    public Rigidbody rb;

    Vector3 prevPosition;

    private void Start()
    {
        currentCounter = initialCounter;
        rb = gameObject.GetComponent<Rigidbody>();
        prevPosition = transform.position;
    }

    private void Update()
    {
        if (gameObject.tag == "Player")
        {
            if (rb.velocity.magnitude > 0)
                PlaySound();
            else if (currentCounter != initialCounter)
                currentCounter = initialCounter;
        }
        else
        {
            Vector3 vel = transform.position - prevPosition;
            float movement = Mathf.Abs(vel.x + vel.y + vel.z);

            if (movement > 0.2f) //I don't trust floats, so adding a threshold to avoid miscalculations to play footstep sounds.
            { 
                PlaySound();
            }
            else
                currentCounter = initialCounter;
        }
    }

    private void PlaySound()
    {
        currentCounter -= Time.deltaTime;

        if (currentCounter <= 0)
        {
            AkSoundEngine.PostEvent(soundName, gameObject);
            currentCounter = initialCounter;
        }
    }
}
